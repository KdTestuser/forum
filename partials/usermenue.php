<?php
if(isset($_SESSION['login_user'])) { ?>
<div class="mn_bar">
    <div id="mn_trigger" class="mn_trigger">
        <span></span>
    </div>
</div>
<div class="user_toolbar">
    <a href="" title="Mein Profil"><span class="icon icon-user"></span>Mein Profil</a>
    <a href="" title="Mitteilungen"><span class="icon icon-letter"></span>Mitteilungen</a>
    <a href="" title="Fragen"><span class="icon icon-question"></span>Fragen</a>
</div>
<?php } ?>