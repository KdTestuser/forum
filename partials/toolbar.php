<div class="toolbar clearfix">
    <div class="left">
        <a href="/forum/index.php" title="Home" alt="Home">
            <img src="/forum/img/logo.png" class="logo" title="" alt=""/>
        </a>
    </div>
    <div class="right">
        <?php
        if(isset($_SESSION['login_user'])) { ?>
            <div class="item">Willkommen <?php echo $_SESSION['login_user']['Username']; ?></div>
            <a href="/forum/php/logout.php" class="item logout"><span class="icon icon-exit"></span>Ausloggen</a>
            <?php
        }
        else { ?>
        <a href="registrieren/registrieren.php" class="item register"><span class="icon icon-user"></span>Registrieren</a>
        <div class="item login"><span class="icon icon-enter"></span>Einloggen</div>
        <?php } ?>
    </div>
    <div class="main_nav">
        <ul class="lvl0">
            <li><a href="" title="Home" alt="Home">Home</a></li>
            <li><a href="" title="Frage stellen" alt="Frage stellen">Frage stellen</a></li>
            <li><a href="" title="News" alt="News">News</a></li>
            <li><a href="" title="Sonstiges" alt="Sonstiges">Sonstiges</a></li>
        </ul>
    </div>
</div>