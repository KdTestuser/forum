<?php
define('WEB_ROOT', 'C:\xampp\htdocs\forum');
require WEB_ROOT."/php/authentication.php";
require WEB_ROOT."/php/database_queries.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require WEB_ROOT . "/partials/includeMetaTags.php" ?>
    <title>forum</title>
    <?php require WEB_ROOT."/partials/includeCSS.php" ?>
</head>
<body>
<?php require WEB_ROOT . "/partials/toolbar.php" ?>
<div class="main clearfix">
    <div class="container full registration">
        <div class="ma_center">
<!--            <h1>Danke für Ihre Registrierung</h1>-->
<!--            <div class="text">Bitte bestätigen Sie noch den Bestätigungslink in der E-Mail, die Ihnen in Kürze zugesendet wird</div>-->
        </div>
    </div>
</div>
<?php require WEB_ROOT . "/partials/login.php" ?>
<?php require WEB_ROOT . "/partials/includeJS.php" ?>
</body>
</html>