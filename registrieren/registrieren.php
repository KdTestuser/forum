<?php
define('WEB_ROOT', 'C:\xampp\htdocs\forum');
require WEB_ROOT."/php/authentication.php";
require WEB_ROOT."/php/database_queries.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require WEB_ROOT . "/partials/includeMetaTags.php" ?>
    <title>forum</title>
    <?php require WEB_ROOT."/partials/includeCSS.php" ?>
</head>
<body>
<?php require WEB_ROOT . "/partials/login.php" ?>
<?php require WEB_ROOT . "/partials/toolbar.php" ?>
<div class="main clearfix">
    <div class="container full registration">
        <h1>Registrierung</h1>
        <form action="" class="form" method="post">
            <div class="row clearfix">
                <label>Benutzername:</label>
                <input type="text" name="rg_username" class="username"/>
            </div>
            <div class="row">
                <label>E-Mail:</label>
                <input type="email" name="rg_email" class="email"/>
            </div>
            <div class="row clearfix">
                <div class="left">
                    <label>Vorname:</label>
                    <input type="text" name="rg_firstname" class="firstname"/>
                </div>
                <div class="right">
                    <label>Nachname:</label>
                    <input type="text" name="rg_lastname" class="lastname"/>
                </div>
            </div>
            <div class="row">
                <label>Passwort:</label>
                <input type="password" name="rg_password" class="password"/>
            </div>
            <div class="row">
                <label>Passwort wiederholen:</label>
                <input type="password" name="rg_password2" class="password"/>
            </div>
            <div class="row">
                <input type="submit"/>
            </div>
        </form>
    </div>
</div>
<?php require WEB_ROOT . "/partials/includeJS.php" ?>
</body>
</html>