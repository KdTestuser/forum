/*
 * JS Hint (http://www.jshint.com) verwenden, um Code zu testen
 * JS Codestyle (https://github.com/airbnb/javascript) muss eingehalten werden
 * Globale Variablen vermeiden (max 1 - 3)
 */

(function($, window, document, undefined) {

    // strict mode
    'use strict';

    // load webfonts
    window.WebFontConfig = {
        google: {
            families: [ 'Open+Sans:300,400,700']
        }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' === document.location.protocol ? 'https' : 'http') +
          '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();

})(jQuery, window, document);
