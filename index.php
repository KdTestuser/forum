<?php
define('WEB_ROOT', 'C:\xampp\htdocs\forum');
require WEB_ROOT."/php/authentication.php";
require WEB_ROOT."/php/database_queries.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require WEB_ROOT . "/partials/includeMetaTags.php" ?>
    <title>forum</title>
    <?php require WEB_ROOT."/partials/includeCSS.php" ?>
</head>
<body>
<?php require WEB_ROOT . "/partials/login.php" ?>
<?php require WEB_ROOT . "/partials/toolbar.php" ?>
<div class="main clearfix">
    <?php require WEB_ROOT . "/partials/usermenue.php" ?>
    <div class="container">
    </div>
</div>
<?php require WEB_ROOT . "/partials/includeJS.php" ?>
</body>
</html>