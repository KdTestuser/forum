<?php
include('config/database.php');
class User{

    public $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    public function getRegistration($rg_username,$rg_email,$rg_firstname,$rg_lastname,$rg_password){
        $pdo = $this->pdo;
        //TODO: Überprüfen ob der Name oder die Email Adresse schon verwendet werden, wenn ja, soll die Registrierung nicht abgeschlossen werden und eine Fehlermeldung soll ausgegeben werden
        //TODO: Vorher ist aber noch eine clientseitige Validierung der Felder durchzuführen (mit Javascript), ob alle Felderkorrekt ausgefüllt worden sind, wenn nicht, soll eine Klasse auf das jeweilige Feld gesetzt werden, die das Feld rot färbt

        $statement = $pdo->prepare("INSERT INTO users (username,email,vorname,nachname,password) values (?,?,?,?,sha2(?,224))");
        $statement->execute(array($rg_username, $rg_email,$rg_firstname,$rg_lastname,$rg_password));
        //TODO: User soll eine Mail erhalten, mit den Bestätigungslink. Dieser Wert soll in der Datenbank abgespeichert werden
        //TODO: Beim Aufruf der Seite sollen diese Parameter ausgelesen werden(am besten du nimmst dafür eine eigene Seite(habe dir eine im Ordner registrieren/danke/erfolgreich.php angelegt) und überprüftst es mit der Datenbank und wenn es erfolgreich ist, soll der Wert eines weiteren Feld in der Datenbank auf 1 gesetzt werden, damit dieser als bestätigt markiert ist.
        header('Location:../registrieren/danke/registrierung.php');
        exit;
    }
    function getLogin($username,$password){
        $pdo = $this->pdo;
        $statement = $pdo->prepare("SELECT * FROM users WHERE username = ? AND password = sha2(?,224)");
        $statement->execute(array($username, $password));
        if($statement->rowCount()){
            $result = $statement->fetch();
            $_SESSION['login_user'] = $result;
            //Login erfolgreich
        }
        else{
            //TODO: wenn Benutzer nicht existiert, dann ist eine Fehlermeldung auszugeben, die schöner aussieht als nur eine alert Box
            echo "<script>alert('Passwort falsch')</script>";
        }
    }
}
if(isset($_POST['rg_username']) && isset($_POST['rg_password'])) {
    $sel = new User($pdo);
    $sel->getRegistration($_POST['rg_username'],$_POST['rg_email'],$_POST['rg_firstname'],$_POST['rg_lastname'],$_POST['rg_password']);
}
if(isset($_POST['lg_username']) && isset($_POST['lg_password'])) {
    $sel = new User($pdo);
    $sel->getLogin($_POST['lg_username'],$_POST['lg_password']);
}

//$conn->close();

